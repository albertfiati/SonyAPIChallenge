package bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

public class SonyAPI {

    public static final int PORT = 54480;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");


    public static Call client(String library, String iPAddress, String body) {
        RequestBody requestBody = RequestBody.create(JSON, body);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .addHeader("Content-Type", "application/json")
                .url(String.format("http://%s:54480/sony/%s/", iPAddress, library))
                .post(requestBody)
                .build();

        return client.newCall(request);
    }
}