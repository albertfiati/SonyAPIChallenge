package bth.sony.hackathon.audioapi.sonyaudioapichallenge.fragments;

import android.app.Fragment;
import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.R;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters.SetAudioMute;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters.SetAudioVolume;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters.SetPlayNextContent;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters.SetPlayPreviousContent;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters.SetPowerStatus;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SelectedSpeaker.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SelectedSpeaker#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SelectedSpeaker extends Fragment {
    public Context mContext;
    public View mView;
    public SeekBar mVolumeSeekBar,
            mVolumeSeekBarAlt;
    public ImageView mMuteButton,
            mMuteButtonAlt,
            mIncreaseVolumeButton,
            mIncreaseVolumeButtonAlt,
            mPauseButton,
            mPlayButton,
            mPauseButtonBig,
            mPlayButtonBig,
            mNextButton,
            mPreviousButton,
            mSlideUp,
            mSlideDown,
            mSpeakerIcon,
            mPowerButton;

    public TextView mSpeakerName,
            mTitleContent,
            mInputSource,
            mAltTitleContent,
            mAltInputSource;

    public LinearLayout mSlideUpView,
            mSlideDownView;
    public int mVolume;

    public Speaker mSpeaker;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle speakerBundle = getArguments();
        mSpeaker = (Speaker) speakerBundle.getSerializable("speaker");

        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_selected_speaker, container, false);

        //TODO:: update this with the current volume of the speaker
        mVolume = mSpeaker.volume.volume;

        mContext = getActivity().getApplicationContext();
        mVolumeSeekBarAlt = mView.findViewById(R.id.volume_seek_bar_alt);
        mVolumeSeekBarAlt.setOnSeekBarChangeListener(modifyVolume);

        mVolumeSeekBarAlt.setMax(mSpeaker.volume.maxVolume);
        mVolumeSeekBarAlt.setProgress(mSpeaker.volume.volume);

        mVolumeSeekBar = mView.findViewById(R.id.volume_seek_bar);
        mVolumeSeekBar.setOnSeekBarChangeListener(modifyVolume);

        mVolumeSeekBar.setMax(mSpeaker.volume.maxVolume);
        mVolumeSeekBar.setProgress(mSpeaker.volume.volume);

        mMuteButtonAlt = (ImageView) mView.findViewById(R.id.mute_alt);
        mMuteButtonAlt.setOnClickListener(muteVolume);

        mMuteButton = (ImageView) mView.findViewById(R.id.mute);
        mMuteButton.setOnClickListener(muteVolume);

        mIncreaseVolumeButtonAlt = (ImageView) mView.findViewById(R.id.volume_up_alt);
        mIncreaseVolumeButtonAlt.setOnClickListener(increaseVolume);

        mIncreaseVolumeButton = (ImageView) mView.findViewById(R.id.volume_up);
        mIncreaseVolumeButton.setOnClickListener(increaseVolume);

        mPlayButtonBig = (ImageView) mView.findViewById(R.id.play_alt);
        mPlayButtonBig.setOnClickListener(play);
        mPlayButton = (ImageView) mView.findViewById(R.id.play);
        mPlayButton.setOnClickListener(play);

        mPauseButtonBig = (ImageView) mView.findViewById(R.id.pause_alt);
        mPauseButtonBig.setOnClickListener(pause);
        mPauseButton = (ImageView) mView.findViewById(R.id.pause);
        mPauseButton.setOnClickListener(pause);

        mSlideUp = (ImageView) mView.findViewById(R.id.slide_up);
        mSlideUp.setOnClickListener(slideUp);

        mSlideDown = (ImageView) mView.findViewById(R.id.slide_down);
        mSlideDown.setOnClickListener(slideDown);

        mPreviousButton = (ImageView) mView.findViewById(R.id.previous);
        mPreviousButton.setOnClickListener(previous);

        mNextButton = (ImageView) mView.findViewById(R.id.next);
        mNextButton.setOnClickListener(next);

        mSlideDownView = (LinearLayout) mView.findViewById(R.id.slide_down_view);
        mSlideUpView = (LinearLayout) mView.findViewById(R.id.slided_up_view);

        mSpeakerName = (TextView) mView.findViewById(R.id.speake_name);
        mSpeakerName.setText(mSpeaker.name);

        mTitleContent = (TextView) mView.findViewById(R.id.title_content);
        mTitleContent.setText(mSpeaker.playingContent.uri);

        mInputSource = (TextView) mView.findViewById(R.id.input_source);
        mInputSource.setText(mSpeaker.playingContent.source);

        mAltTitleContent = (TextView) mView.findViewById(R.id.alt_content_title);
        mAltTitleContent.setText(mSpeaker.playingContent.uri);

        mAltInputSource = (TextView) mView.findViewById(R.id.alt_insput_source);
        mAltInputSource.setText(mSpeaker.playingContent.source);

        mSpeakerIcon = (ImageView) mView.findViewById(R.id.speaker_icon);
        mSpeakerIcon.setOnLongClickListener(turnOff);
        //mSpeakerIcon.setOnClickListener(standby);

        mPowerButton = (ImageView) mView.findViewById(R.id.power_button);
        mPowerButton.setOnClickListener(turnOn);

        togglePowerView();

        return mView;
    }

    private void togglePowerView() {
        if (mSpeaker.power.status.toLowerCase().equals("active")) {
            mSpeakerIcon.setVisibility(View.VISIBLE);
            mPowerButton.setVisibility(View.GONE);
        } else {
            mPowerButton.setVisibility(View.VISIBLE);
            mSpeakerIcon.setVisibility(View.GONE);
        }
    }

    private void setSpeakerVolume(int volume) {
        mVolume = volume;
        SetAudioVolume.set(mSpeaker, volume);
        mVolumeSeekBar.setProgress(mVolume);
    }

    SeekBar.OnSeekBarChangeListener modifyVolume = new SeekBar.OnSeekBarChangeListener() {
        int progress = 0;

        @Override
        public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
            progress = progresValue;
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            progress = seekBar.getProgress();
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            setSpeakerVolume(progress);
        }
    };

    ImageView.OnClickListener muteVolume = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SetAudioMute.set(mSpeaker);
            Toast.makeText(mContext, "Mute is " + mSpeaker.volume.mute, Toast.LENGTH_SHORT).show();
        }
    };

    ImageView.OnClickListener increaseVolume = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mVolumeSeekBar.getProgress() < 100)
                setSpeakerVolume(mVolumeSeekBar.getProgress() + 1);
        }
    };

    ImageView.OnClickListener play = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag().toString().equals("big")) {
                mPlayButtonBig.setVisibility(View.GONE);
                mPauseButtonBig.setVisibility(View.VISIBLE);
            } else {
                mPlayButton.setVisibility(View.GONE);
                mPauseButton.setVisibility(View.VISIBLE);
            }

            Toast.makeText(mContext, "Play", Toast.LENGTH_SHORT).show();
        }
    };

    ImageView.OnClickListener pause = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag().toString().equals("big")) {
                mPauseButtonBig.setVisibility(View.GONE);
                mPlayButtonBig.setVisibility(View.VISIBLE);
            } else {
                mPauseButton.setVisibility(View.GONE);
                mPlayButton.setVisibility(View.VISIBLE);
            }
            Toast.makeText(mContext, "Paused", Toast.LENGTH_SHORT).show();
        }
    };

    ImageView.OnClickListener next = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SetPlayNextContent.set(mSpeaker);
            Toast.makeText(mContext, "Next", Toast.LENGTH_SHORT).show();
        }
    };

    ImageView.OnClickListener previous = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SetPlayPreviousContent.set(mSpeaker);
            Toast.makeText(mContext, "Previous", Toast.LENGTH_SHORT).show();
        }
    };

    ImageView.OnClickListener slideUp = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mSlideDownView.setVisibility(View.GONE);
            mSlideUpView.setVisibility(View.VISIBLE);
        }
    };

    ImageView.OnClickListener slideDown = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mSlideUpView.setVisibility(View.GONE);
            mSlideDownView.setVisibility(View.VISIBLE);
        }
    };

    ImageView.OnClickListener turnOn = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SetPowerStatus.set(mSpeaker, "turnon");
            togglePowerView();
        }
    };

    ImageView.OnClickListener standby = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SetPowerStatus.set(mSpeaker, "standby");
            togglePowerView();
        }
    };

    ImageView.OnLongClickListener turnOff = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            SetPowerStatus.set(mSpeaker, "turnoff");
            togglePowerView();
            return false;
        }
    };
}
