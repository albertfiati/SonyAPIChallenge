package bth.sony.hackathon.audioapi.sonyaudioapichallenge.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Speaker implements Serializable {
    public String lastPowerOnTime;
    public String initialPowerOnTime;
    public String bleID;
    public String blueAddr;
    public String iconUrl;
    public String esn;
    public String duid;
    public String deviceID;
    public String helpUrl;
    public String cid;
    public String area;
    public String generation;
    public String serial;
    public String model;
    public String language;
    public String region;
    public String product;
    public String name;
    public String IPAddress;
    public String bdAddr;
    public String wirelessMacAddr;
    public String macAddr;
    public String ssid;
    public String version;

    public Power power;
    public Volume volume;
    public PlayingContent playingContent;
    public List<EqualizerSettings> equalizers = new ArrayList<>();

    public Speaker() {

    }

    public Speaker(String name, String IPAddress) {
        this.name = name;
        this.IPAddress = IPAddress;
    }
}
