package bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters;

import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Candidate;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.EqualizerSettings;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.PlayingContent;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.StateInfo;

public class GetPlayingContentInfo {
    public static final String LIBRARY = "avContent";
    public static final String BODY = "{\"method\":\"getPlayingContentInfo\",\"id\":37,\"params\":[{\"output\":\"\"}],\"version\":\"1.2\"}";


    public static void get(Speaker speaker) {
        try {
            Response response = SonyAPI.client(LIBRARY, speaker.IPAddress, BODY).execute();

            JSONObject jsonRes = new JSONObject(response.body().string().toString());
            JSONArray playingContentInfo = jsonRes.getJSONArray("result").getJSONArray(0);

            for (int i = 0; i < playingContentInfo.length(); i++) {
                try {
                    JSONObject content = playingContentInfo.getJSONObject(i);

                    PlayingContent playingContent = new PlayingContent();
                    playingContent.uri = content.getString("uri");
                    playingContent.contentKind = content.getString("contentKind");
                    playingContent.parentUri = content.getString("parentUri");
                    playingContent.source = content.getString("source");

                    StateInfo stateInfo = new StateInfo();
                    JSONObject _stateInfo = content.getJSONObject("stateInfo");

                    stateInfo.state = _stateInfo.getString("state");
                    stateInfo.supplement = _stateInfo.getString("supplement");

                    playingContent.stateInfo = stateInfo;

                    speaker.playingContent = playingContent;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}