package bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters;

import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Power;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;

public class SetPowerStatus {
    public static final String LIBRARY = "system";
    public static final String TURNON = "{\"method\":\"setPowerStatus\",\"id\":55,\"params\":[{\"status\":\"active\"}],\"version\":\"1.1\"}";
    public static final String TURNOFF = "{\"method\":\"setPowerStatus\",\"id\":55,\"params\":[{\"status\":\"off\"}],\"version\":\"1.1\"}";
    public static final String STANDBY = "{\"method\":\"setPowerStatus\",\"id\":55,\"params\":[{\"status\":\"standby\"}],\"version\":\"1.1\"}";


    public static void set(Speaker speaker, String option) {
        try {
            Response response;
            String status;

            switch (option) {
                case "turnon":
                    status = "active";
                    response = SonyAPI.client(LIBRARY, speaker.IPAddress, TURNON).execute();
                    break;
                case "turnoff":
                    status = "off";
                    response = SonyAPI.client(LIBRARY, speaker.IPAddress, TURNOFF).execute();
                    break;
                default:
                    status = "standby";
                    response = SonyAPI.client(LIBRARY, speaker.IPAddress, STANDBY).execute();
                    break;
            }


            if (response.code() == 200)
                speaker.power.status = status;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}