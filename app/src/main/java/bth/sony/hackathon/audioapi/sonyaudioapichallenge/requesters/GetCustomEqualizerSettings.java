package bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters;


import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Candidate;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.EqualizerSettings;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;

public class GetCustomEqualizerSettings {
    public static final String LIBRARY = "audio";
    public static final String BODY = "{\"method\":\"getCustomEqualizerSettings\",\"id\":11,\"params\":[{\"target\":\"\"}],\"version\":\"1.0\"}";


    public static void get(Speaker speaker) {
        try {
            Response response = SonyAPI.client(LIBRARY, speaker.IPAddress, BODY).execute();

            JSONObject jsonRes = new JSONObject(response.body().string().toString());
            JSONArray settings = jsonRes.getJSONArray("result").getJSONArray(0);

            for (int i = 0; i < settings.length(); i++) {
                try {
                    JSONObject setting = settings.getJSONObject(i);

                    EqualizerSettings e = new EqualizerSettings();
                    e.target = setting.getString("target");
                    e.titleTextID = setting.getString("titleTextID");
                    e.type = setting.getString("type");
                    e.isAvailable = setting.getBoolean("isAvailable");
                    e.currentValue = setting.getString("currentValue");

                    Candidate candidate = new Candidate();
                    JSONArray candidateArr = setting.getJSONArray("candidate");
                    JSONObject candidateObj = candidateArr.getJSONObject(0);

                    candidate.min = candidateObj.getInt("min");
                    candidate.max = candidateObj.getInt("max");
                    candidate.step = candidateObj.getInt("step");
                    candidate.isAvailable = candidateObj.getBoolean("isAvailable");

                    e.candidate = candidate;

                    speaker.equalizers.add(e);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}