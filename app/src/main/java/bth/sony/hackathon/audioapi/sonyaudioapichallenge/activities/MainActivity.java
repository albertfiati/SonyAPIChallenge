package bth.sony.hackathon.audioapi.sonyaudioapichallenge.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.format.Formatter;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.R;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.fragments.AvailableSpeakers;

public class MainActivity extends FragmentActivity {
    public static final int SPLASH_TIME_OUT = 1200;
    public static final int INTERNET = 6000;
    public static final int ACCESS_NETWORK_STATE = 7000;
    public static final int ACCESS_WIFI_STATE = 8000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);

        int version = Build.VERSION.SDK_INT;

        if (version > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (version > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.INTERNET}, INTERNET);
            } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, ACCESS_NETWORK_STATE);
            } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_WIFI_STATE}, ACCESS_WIFI_STATE);
            }
        }

        super.onCreate(savedInstanceState);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                TextView credit = (TextView) findViewById(R.id.credit);
                credit.setVisibility(View.INVISIBLE);

                Toolbar actionBar = (Toolbar) findViewById(R.id.my_toolbar);
                actionBar.setVisibility(View.VISIBLE);

                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.mainFragmentContainer, new AvailableSpeakers())
                        .commit();


                setActionBar(actionBar);

            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case INTERNET:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Toast.makeText(MainActivity.this, "Internet access granted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Internet access not granted", Toast.LENGTH_SHORT).show();
                }
                return;
            case ACCESS_NETWORK_STATE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Toast.makeText(MainActivity.this, "network access granted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "network access not granted", Toast.LENGTH_SHORT).show();
                }
                return;
            case ACCESS_WIFI_STATE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Toast.makeText(MainActivity.this, "wifi access granted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "wifi access not granted", Toast.LENGTH_SHORT).show();
                }
                return;
            default:
                break;
        }
    }
}
