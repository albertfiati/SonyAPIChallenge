package bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters;

import com.squareup.okhttp.Response;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;


public class SetPlayNextContent {
    public static final String LIBRARY = "avContent";
    public static final String BODY = "{\"method\":\"setPlayNextContent\",\"id\":17,\"params\":[{\"output\":\"\"}],\"version\":\"1.0\"}";

    public static void set(Speaker speaker) {
        try {
            Response response = SonyAPI.client(LIBRARY, speaker.IPAddress, BODY).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}