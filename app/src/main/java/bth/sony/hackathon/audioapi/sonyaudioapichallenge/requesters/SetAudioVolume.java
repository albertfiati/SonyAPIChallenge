package bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters;

import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Volume;

public class SetAudioVolume {
    public static final String LIBRARY = "audio";

    public static void set(Speaker speaker, int volume) {
        try {
            String BODY = String.format("{\"method\":\"setAudioVolume\",\"id\":98,\"params\":[{\"volume\":\"%d\",\"output\":\"extOutput:zone?zone=2\"}],\"version\":\"1.1\"}", volume);
            Response response = SonyAPI.client(LIBRARY, speaker.IPAddress, BODY).execute();

            if (response.code() == 200)
                speaker.volume.volume = volume;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
