package bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters;

import com.squareup.okhttp.Response;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;

public class SetPlayPreviousContent {
    public static final String LIBRARY = "avContent";
    public static final String BODY = "{\"method\":\"setPlayPreviousContent\",\"id\":82,\"params\":[{\"output\":\"\"}],\"version\":\"1.0\"}";

    public static void set(Speaker speaker) {
        try {
            Response response = SonyAPI.client(LIBRARY, speaker.IPAddress, BODY).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}