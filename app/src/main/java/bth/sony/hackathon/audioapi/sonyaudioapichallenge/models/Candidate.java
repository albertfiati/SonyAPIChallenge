package bth.sony.hackathon.audioapi.sonyaudioapichallenge.models;

public class Candidate {
    public int max;
    public int min;
    public int step;
    public boolean isAvailable;
}
