package bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters;

import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.PlayingContent;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.StateInfo;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Volume;

public class GetVolumeInformation {
    public static final String LIBRARY = "audio";
    public static final String BODY = "{\"method\":\"getVolumeInformation\",\"id\":33,\"params\":[{\"output\":\"extOutput:zone?zone=2\"}],\"version\":\"1.1\"}";


    public static void get(Speaker speaker) {
        try {
            Response response = SonyAPI.client(LIBRARY, speaker.IPAddress, BODY).execute();

            JSONObject jsonRes = new JSONObject(response.body().string().toString());
            JSONArray _volume = jsonRes.getJSONArray("result").getJSONArray(0);

            for (int i = 0; i < _volume.length(); i++) {
                try {
                    JSONObject vol = _volume.getJSONObject(i);

                    Volume volume = new Volume();
                    volume.maxVolume = vol.getInt("maxVolume");
                    volume.minVolume = vol.getInt("minVolume");
                    volume.volume = vol.getInt("volume");
                    volume.step = vol.getInt("step");
                    volume.mute = vol.getString("mute");

                    speaker.volume = volume;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}