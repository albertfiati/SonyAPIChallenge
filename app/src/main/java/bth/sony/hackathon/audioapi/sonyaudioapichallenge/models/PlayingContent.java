package bth.sony.hackathon.audioapi.sonyaudioapichallenge.models;

public class PlayingContent {
    public String uri;
    public String contentKind;
    public String parentUri;
    public String source;
    public StateInfo stateInfo;
}
