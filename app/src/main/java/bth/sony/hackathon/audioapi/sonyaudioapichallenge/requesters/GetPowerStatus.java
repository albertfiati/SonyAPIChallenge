package bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters;

import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Power;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;

public class GetPowerStatus {
    public static final String LIBRARY = "system";
    public static final String BODY = "{\"method\":\"getPowerStatus\",\"id\":50,\"params\":[],\"version\":\"1.1\"}";


    public static void get(Speaker speaker) {
        try {
            Response response = SonyAPI.client(LIBRARY, speaker.IPAddress, BODY).execute();
            JSONObject jsonRes = new JSONObject(response.body().string().toString());
            JSONArray jsonArr = jsonRes.getJSONArray("result");
            JSONObject properties = jsonArr.getJSONObject(0);

            Power power = new Power();
            power.status = properties.getString("status");
            speaker.power = power;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}