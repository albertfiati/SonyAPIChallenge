package bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters;

import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;

public class GetSystemInformation {
    public static final String LIBRARY = "system";
    public static final String BODY = "{\"method\":\"getSystemInformation\",\"id\":65,\"params\":[],\"version\":\"1.4\"}";


    public static Speaker get(String iPAddress) {
        final Speaker speaker = new Speaker();
        speaker.IPAddress = iPAddress;

        try {
            Response response = SonyAPI.client(LIBRARY, iPAddress, BODY).execute();
            JSONObject jsonRes = new JSONObject(response.body().string().toString());
            JSONArray jsonArr = jsonRes.getJSONArray("result");
            JSONObject properties = jsonArr.getJSONObject(0);

            speaker.bleID = properties.getString("bleID");
            speaker.name = properties.getString("bleID");
            speaker.bdAddr = properties.getString("bdAddr");
            speaker.wirelessMacAddr = properties.getString("wirelessMacAddr");
            speaker.macAddr = properties.getString("macAddr");
            speaker.ssid = properties.getString("ssid");
            speaker.version = properties.getString("version");

            GetCustomEqualizerSettings.get(speaker);
            GetPlayingContentInfo.get(speaker);
            GetVolumeInformation.get(speaker);
            GetPowerStatus.get(speaker);

            System.out.println(speaker.power.status);

            //System.out.println(speaker.volume.maxVolume);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return speaker;
    }
}

/*    .enqueue(new Callback() {
    @Override
    public void onFailure(Request request, IOException e) {

    }

    @Override
    public void onResponse(Response response) throws IOException {
        if (response.isSuccessful()) {
            try {
                JSONObject jsonRes = new JSONObject(response.body().string().toString());
                JSONArray jsonArr = jsonRes.getJSONArray("result");

                JSONObject properties = jsonArr.getJSONObject(0);

                //speaker.lastPowerOnTime = properties.get("lastPowerOnTime").toString();
                //speaker.initialPowerOnTime = properties.get("initialPowerOnTime").toString();
                speaker.bleID = properties.get("bleID").toString();
                speaker.name = properties.get("bleID").toString();
                //speaker.blueAddr = properties.get("blueAddr").toString();
                //speaker.iconUrl = properties.get("iconUrl").toString();
                //speaker.esn = properties.get("esn").toString();
                //speaker.duid = properties.get("duid").toString();
                //speaker.deviceID = properties.get("deviceID").toString();
                //speaker.helpUrl = properties.get("helpUrl").toString();
                //speaker.cid = properties.get("cid").toString();
                //speaker.area = properties.get("area").toString();
                //speaker.generation = properties.get("generation").toString();
                //speaker.serial = properties.get("serial").toString();
                //speaker.model = properties.get("model").toString();
                //speaker.language = properties.get("language").toString();
                //speaker.region = properties.get("region").toString();
                //speaker.product = properties.get("product").toString();
                speaker.bdAddr = properties.get("bdAddr").toString();
                speaker.wirelessMacAddr = properties.get("wirelessMacAddr").toString();
                speaker.macAddr = properties.get("macAddr").toString();
                speaker.ssid = properties.get("ssid").toString();
                speaker.version = properties.get("version").toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

        }
    }
});*/
