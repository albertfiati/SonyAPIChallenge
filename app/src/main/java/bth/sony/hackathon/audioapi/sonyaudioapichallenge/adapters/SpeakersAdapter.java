package bth.sony.hackathon.audioapi.sonyaudioapichallenge.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.R;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;

public class SpeakersAdapter extends RecyclerView.Adapter<SpeakersAdapter.MyViewHolder> {
    private List<Speaker> speakersList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, iPAddress;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
        }
    }


    public SpeakersAdapter(List<Speaker> speakersList) {
        this.speakersList = speakersList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.speaker_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Speaker speaker = speakersList.get(position);
        holder.name.setText(speaker.name);
    }

    @Override
    public int getItemCount() {
        return speakersList.size();
    }
}
