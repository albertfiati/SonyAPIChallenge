package bth.sony.hackathon.audioapi.sonyaudioapichallenge.models;

public class Volume {
    public int volume;
    public int step;
    public String mute;
    public int maxVolume;
    public int minVolume;
}
