package bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters;

import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;

public class GetCurrentExternalTerminalsStatus {
    public static final String LIBRARY = "avContent";
    public static final String BODY = "{\"method\":\"getCurrentExternalTerminalsStatus\",\"id\":65,\"params\":[],\"version\":\"1.0\"}";


    public static Speaker get(Speaker speaker) {
        try {
            Response response = SonyAPI.client(LIBRARY, speaker.IPAddress, BODY).execute();
            JSONObject jsonRes = new JSONObject(response.body().string().toString());
            JSONArray jsonArr = jsonRes.getJSONArray("result");
            JSONObject properties = jsonArr.getJSONObject(0);
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return speaker;
    }
}