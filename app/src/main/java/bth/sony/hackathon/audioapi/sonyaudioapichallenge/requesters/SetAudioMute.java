package bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters;

import com.squareup.okhttp.Response;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;

public class SetAudioMute {
    public static final String LIBRARY = "audio";

    public static void set(Speaker speaker) {
        try {
            String oldState = speaker.volume.mute;

            String BODY = String.format("{\"method\":\"setAudioMute\",\"id\":601,\"params\":[{\"mute\":\"%s\"}],\"version\":\"1.1\"}", oldState.toLowerCase().equals("off") ? "on" : "off");
            Response response = SonyAPI.client(LIBRARY, speaker.IPAddress, BODY).execute();

            if (response.code() == 200)
                speaker.volume.mute = oldState.toLowerCase().equals("off") ? "on" : "off";

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}