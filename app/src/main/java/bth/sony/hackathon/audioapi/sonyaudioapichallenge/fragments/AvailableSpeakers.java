package bth.sony.hackathon.audioapi.sonyaudioapichallenge.fragments;

import android.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bth.sony.hackathon.audioapi.sonyaudioapichallenge.R;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.adapters.SpeakersAdapter;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.listeners.RecyclerViewClickLIstener;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.models.Speaker;
import bth.sony.hackathon.audioapi.sonyaudioapichallenge.requesters.GetSystemInformation;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AvailableSpeakers.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AvailableSpeakers#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AvailableSpeakers extends Fragment {

    private static final int PORT = 54480;

    public Context mContext;
    public RecyclerView mRecyclerView;
    public List<Speaker> mSpeakerList = new ArrayList<>();
    public SpeakersAdapter mSpeakerAdapter;
    public RecyclerView.LayoutManager mLayoutManager;
    public View mView;
    ImageView mRefresh;

    public NetworkInterface mInterface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getActivity().getApplicationContext();

        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_available_speakers, container, false);

        mRecyclerView = (RecyclerView) mView.findViewById(R.id.speaker_list_reccycler_view);
        mSpeakerAdapter = new SpeakersAdapter(mSpeakerList);
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(mSpeakerAdapter);
        mRecyclerView.addOnItemTouchListener(selectSpeaker);

        mRefresh = (ImageView) mView.findViewById(R.id.reloadSpeakers);
        mRefresh.setOnClickListener(reScan);

        scanNetworkForSpeakers();

        return mView;
    }

    RecyclerView.OnItemTouchListener selectSpeaker = new RecyclerViewClickLIstener(mContext, mRecyclerView, new RecyclerViewClickLIstener.ClickListener() {
        @Override
        public void onClick(View view, int position) {
            Speaker speaker = mSpeakerList.get(position);
            //Toast.makeText(mContext, speaker.name + " is selected!", Toast.LENGTH_SHORT).show();

            Bundle speakerBundle = new Bundle();
            speakerBundle.putSerializable("speaker", speaker);

            Fragment selectedSpeaker = new SelectedSpeaker();
            selectedSpeaker.setArguments(speakerBundle);

            getActivity().getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.mainFragmentContainer, selectedSpeaker)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onLongClick(View view, int position) {

        }
    });

    private void scanNetworkForSpeakers() {
        if (isNetworkAvailable()) {
            Map<String, String> speakerAddresses = this.getListOfSpeakerIPs(this.getSubnet());

            if (speakerAddresses.size() > 0) {
                mSpeakerList.clear();

                for (Map.Entry<String, String> speaker : speakerAddresses.entrySet()) {
                    Speaker _speaker = GetSystemInformation.get(speaker.getKey());
                    mSpeakerList.add(_speaker);
                }
            }

            if (mSpeakerList.size() > 0) {
                mRefresh.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
            } else {
                mRecyclerView.setVisibility(View.GONE);
                mRefresh.setVisibility(View.VISIBLE);
                Toast.makeText(mContext, "No speakers found on the network", Toast.LENGTH_SHORT).show();
            }

            mSpeakerAdapter.notifyDataSetChanged();
        } else {
            Toast.makeText(mContext, "Sorry... No network connection", Toast.LENGTH_SHORT).show();
        }
    }

    ImageView.OnClickListener reScan = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast.makeText(mContext, "Rescanning network for speakers", Toast.LENGTH_SHORT).show();
            scanNetworkForSpeakers();
        }
    };

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(mContext.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    private String getSubnet() {
        String hostString = getIPMyAddress(true);
        return hostString.substring(hostString.indexOf("/") + 1, hostString.lastIndexOf(".") + 1);
    }

    private Map<String, String> getListOfSpeakerIPs(String subnet) {
        Map<String, String> validIPAddresses = new HashMap<>();
        String _ipAddress;

        InetAddress IPAddress;

        for (int i = 1; i < 255; i++) {
            _ipAddress = subnet + i;

            try {
                IPAddress = InetAddress.getByName(_ipAddress);

                if (IPAddress.isReachable(mInterface, 600, 10)) {
                    try {
                        Socket socket = new Socket(IPAddress, PORT);

                        if (socket.isConnected()) {
                            validIPAddresses.put(IPAddress.getCanonicalHostName(), _ipAddress);
                        }
                    } catch (Exception ex) {
                        System.out.println("Socket connection:: " + ex.getMessage());
                    }
                }
            } catch (UnknownHostException e) {
                //e.printStackTrace();
                //System.out.println("getListOfSpeakerIPs - UnknownHostException: " + e.getMessage());
            } catch (IOException e) {
                //e.printStackTrace();
                //System.out.println("getListOfSpeakerIPs - IOException: " + e.getMessage());
            }
        }

        return validIPAddresses;
    }

    public String getIPMyAddress(boolean useIPv4) {
        /*WifiManager wm = (WifiManager) getActivity().getSystemService(getActivity().WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());*/

        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());

            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4) {
                                mInterface = intf;
                                return sAddr;
                            }
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions

        return "";
    }
}
