package bth.sony.hackathon.audioapi.sonyaudioapichallenge.models;

public class EqualizerSettings {
    public String target;
    public String titleTextID;
    public String type;
    public boolean isAvailable;
    public String currentValue;
    public Candidate candidate;
}
